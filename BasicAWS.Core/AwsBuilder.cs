namespace BasicAWS.Core
{
    public class AwsBuilder
    {
        public AwsResource CreateAws(string awsAccessKey, string awsSecretKey, string region, string bucketName)
        {
            var aws = new Aws(awsAccessKey, awsSecretKey, region, bucketName);
            return new AwsResource(aws);
        }

        public class AwsResource 
        {
            private readonly Aws _aws;

            public AwsResource(Aws aws)
            {
                _aws = aws;
            }


            public CalculateData Put(string contentType, string objectName, byte[] data)
            {
                return _aws.PutCalculate(contentType, objectName, data);
            }


            public CalculateData Get(string objectName)
            {
                return _aws.GetCalculate(objectName);
            }



        }
    }
}