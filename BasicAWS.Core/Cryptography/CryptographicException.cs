﻿using System;

namespace BasicAWS.Core.Cryptography
{
    public class CryptographicException : Exception
    {
        public CryptographicException()
        {
        }

        public CryptographicException(string message) : base(message)
        {
        }
    }
}
