﻿using System;

namespace BasicAWS.Core.Cryptography
{
    public class CryptographicUnexpectedOperationException : Exception
    {
        public CryptographicUnexpectedOperationException()
        {
        }

        public CryptographicUnexpectedOperationException(string message) : base(message)
        {
        }
    }
}
