﻿using System.Runtime.InteropServices;

namespace BasicAWS.Core.Cryptography
{
    [ComVisible(true)]
    public abstract class SHA256 : HashAlgorithm
    {

        protected SHA256()
        {
            HashSizeValue = 256;
        }

        public static new SHA256 Create()
        {
            return Create("SHA256");
        }

        public static new SHA256 Create(string hashName)
        {
            return (SHA256)CryptoConfig.CreateFromName(hashName);
        }
    }
}
