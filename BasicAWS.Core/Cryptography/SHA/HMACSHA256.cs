﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace BasicAWS.Core.Cryptography
{
    [ComVisible(true)]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class HMACSHA256 : HMAC
    {

        public HMACSHA256()
            : this(KeyBuilder.Key(8))
        {
        }

        public HMACSHA256(byte[] key)
        {
            HashName = "SHA256";
            HashSizeValue = 256;
            Key = key;
        }

        public override sealed byte[] Key
        {
            get { return base.Key; }
            set { base.Key = value; }
        }
    }
}
