﻿using System;
using System.Runtime.InteropServices;

namespace BasicAWS.Core.Cryptography
{
    [ComVisible(true)]
    public class SHA256Managed : SHA256
    {

        private const int BlockSizeBytes = 64;
        private readonly uint[] _h;
        private ulong _count;
        private readonly byte[] _processingBuffer; // Used to start data when passed less than a block worth.
        private int _processingBufferCount; // Counts how much data we have stored that still needs processed.
        private readonly uint[] _buff;

        public SHA256Managed()
        {
            _h = new uint[8];
            _processingBuffer = new byte[BlockSizeBytes];
            _buff = new uint[64];
            Initialize();
        }

        protected override void HashCore(byte[] rgb, int ibStart, int cbSize)
        {
            int i;
            State = 1;

            if (_processingBufferCount != 0)
            {
                if (cbSize < (BlockSizeBytes - _processingBufferCount))
                {
                    Buffer.BlockCopy(rgb, ibStart, _processingBuffer, _processingBufferCount, cbSize);
                    _processingBufferCount += cbSize;
                    return;
                }
                i = (BlockSizeBytes - _processingBufferCount);
                Buffer.BlockCopy(rgb, ibStart, _processingBuffer, _processingBufferCount, i);
                ProcessBlock(_processingBuffer, 0);
                _processingBufferCount = 0;
                ibStart += i;
                cbSize -= i;
            }

            for (i = 0; i < cbSize - cbSize%BlockSizeBytes; i += BlockSizeBytes)
            {
                ProcessBlock(rgb, ibStart + i);
            }

            if (cbSize%BlockSizeBytes == 0) return;
            Buffer.BlockCopy(rgb, cbSize - cbSize%BlockSizeBytes + ibStart, _processingBuffer, 0, cbSize%BlockSizeBytes);
            _processingBufferCount = cbSize%BlockSizeBytes;
        }

        protected override byte[] HashFinal()
        {
            var hash = new byte[32];
            int i;

            ProcessFinalBlock(_processingBuffer, 0, _processingBufferCount);

            for (i = 0; i < 8; i++)
            {
                int j;
                for (j = 0; j < 4; j++)
                {
                    hash[i*4 + j] = (byte) (_h[i] >> (24 - j*8));
                }
            }

            State = 0;
            return hash;
        }

        public override sealed void Initialize()
        {
            _count = 0;
            _processingBufferCount = 0;

            _h[0] = 0x6A09E667;
            _h[1] = 0xBB67AE85;
            _h[2] = 0x3C6EF372;
            _h[3] = 0xA54FF53A;
            _h[4] = 0x510E527F;
            _h[5] = 0x9B05688C;
            _h[6] = 0x1F83D9AB;
            _h[7] = 0x5BE0CD19;
        }

        private void ProcessBlock(byte[] inputBuffer, int inputOffset)
        {
            uint a, b, c, d, e, f, g, h;
            uint t1, t2;
            int i;
            var k1 = SHAConstants.K1;
            var uints = _buff;

            _count += BlockSizeBytes;

            for (i = 0; i < 16; i++)
            {
                uints[i] = (uint) (((inputBuffer[inputOffset + 4*i]) << 24)
                                   | ((inputBuffer[inputOffset + 4*i + 1]) << 16)
                                   | ((inputBuffer[inputOffset + 4*i + 2]) << 8)
                                   | ((inputBuffer[inputOffset + 4*i + 3])));
            }


            for (i = 16; i < 64; i++)
            {
                t1 = uints[i - 15];
                t1 = (((t1 >> 7) | (t1 << 25)) ^ ((t1 >> 18) | (t1 << 14)) ^ (t1 >> 3));

                t2 = uints[i - 2];
                t2 = (((t2 >> 17) | (t2 << 15)) ^ ((t2 >> 19) | (t2 << 13)) ^ (t2 >> 10));
                uints[i] = t2 + uints[i - 7] + t1 + uints[i - 16];
            }

            a = _h[0];
            b = _h[1];
            c = _h[2];
            d = _h[3];
            e = _h[4];
            f = _h[5];
            g = _h[6];
            h = _h[7];

            for (i = 0; i < 64; i++)
            {
                t1 = h + (((e >> 6) | (e << 26)) ^ ((e >> 11) | (e << 21)) ^ ((e >> 25) | (e << 7))) +
                     ((e & f) ^ (~e & g)) + k1[i] + uints[i];

                t2 = (((a >> 2) | (a << 30)) ^ ((a >> 13) | (a << 19)) ^ ((a >> 22) | (a << 10)));
                t2 = t2 + ((a & b) ^ (a & c) ^ (b & c));
                h = g;
                g = f;
                f = e;
                e = d + t1;
                d = c;
                c = b;
                b = a;
                a = t1 + t2;
            }

            _h[0] += a;
            _h[1] += b;
            _h[2] += c;
            _h[3] += d;
            _h[4] += e;
            _h[5] += f;
            _h[6] += g;
            _h[7] += h;
        }

        private void ProcessFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
        {
            ulong total = _count + (ulong) inputCount;
            int paddingSize = (56 - (int) (total%BlockSizeBytes));

            if (paddingSize < 1)
                paddingSize += BlockSizeBytes;

            byte[] fooBuffer = new byte[inputCount + paddingSize + 8];

            for (int i = 0; i < inputCount; i++)
            {
                fooBuffer[i] = inputBuffer[i + inputOffset];
            }

            fooBuffer[inputCount] = 0x80;
            for (int i = inputCount + 1; i < inputCount + paddingSize; i++)
            {
                fooBuffer[i] = 0x00;
            }

            // I deal in bytes. The algorithm deals in bits.
            ulong size = total << 3;
            AddLength(size, fooBuffer, inputCount + paddingSize);
            ProcessBlock(fooBuffer, 0);

            if (inputCount + paddingSize + 8 == 128)
            {
                ProcessBlock(fooBuffer, 64);
            }
        }

        internal void AddLength(ulong length, byte[] buffer, int position)
        {
            buffer[position++] = (byte) (length >> 56);
            buffer[position++] = (byte) (length >> 48);
            buffer[position++] = (byte) (length >> 40);
            buffer[position++] = (byte) (length >> 32);
            buffer[position++] = (byte) (length >> 24);
            buffer[position++] = (byte) (length >> 16);
            buffer[position++] = (byte) (length >> 8);
            buffer[position] = (byte) (length);
        }
    }
}
