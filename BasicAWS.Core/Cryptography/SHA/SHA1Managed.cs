﻿using System.Runtime.InteropServices;

namespace BasicAWS.Core.Cryptography
{
    [ComVisible(true)]
    public class SHA1Managed : SHA1
    {

        private readonly SHA1Internal _sha;

        public SHA1Managed()
        {
            _sha = new SHA1Internal();
        }

        protected override void HashCore(byte[] rgb, int ibStart, int cbSize)
        {
            State = 1;
            _sha.HashCore(rgb, ibStart, cbSize);
        }

        protected override byte[] HashFinal()
        {
            State = 0;
            return _sha.HashFinal();
        }

        public override void Initialize()
        {
            _sha.Initialize();
        }
    }
}
