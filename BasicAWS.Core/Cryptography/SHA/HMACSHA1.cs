﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BasicAWS.Core.Cryptography
{
    [ComVisible(true)]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class HMACSHA1 : HMAC
    {

        public HMACSHA1()
            : this(KeyBuilder.Key(8))
        {
        }

        public HMACSHA1(byte[] key)
        {
            HashName = "SHA1";
            HashSizeValue = 160;
            Key = key;
        }

        public HMACSHA1(byte[] key, bool useManagedSha1)
        {
            HashName = "SHA1" + (useManagedSha1 ? "Managed" : "CryptoServiceProvider");
            HashSizeValue = 160;
            Key = key;
        }

        public override sealed byte[] Key
        {
            get { return base.Key; }
            set { base.Key = value; }
        }
    }
}
