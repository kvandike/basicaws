﻿using System;
using System.Collections.Generic;

namespace BasicAWS.Core.Cryptography
{
    public class CryptoConfig
    {

        private static readonly object LockObject;
        private static IDictionary<string, Type> _algorithms;



        private const string NameSHA1A = "SHA";
        private const string NameSHA1B = "SHA1";
        private const string NameSHA1D = "HashAlgorithm";
        private const string NameHMACSHA1 = "HMACSHA1";
        private const string NameHMACSHA256 = "HMACSHA256";
        private const string NameSHA256A = "SHA256";
        private const string NameSHA256B = "SHA-256";


        static CryptoConfig()
        {
            LockObject = new object();
        }

        private static void Initialize()
        {
            _algorithms = new Dictionary<string, Type>
            {
                {NameHMACSHA256, typeof (HMACSHA256)},
                {NameHMACSHA1, typeof (HMACSHA1)},
                {NameSHA256A, typeof (SHA256Managed)},
                {NameSHA256B, typeof (SHA256Managed)},
                {NameSHA1A, typeof (SHA1CryptoServiceProvider)},
                {NameSHA1B, typeof (SHA1CryptoServiceProvider)},
                {NameSHA1D, typeof (SHA1CryptoServiceProvider)}

            };
        }


        public static object CreateFromName(string name)
        {
            return CreateFromName(name, null);
        }

        public static object CreateFromName(string name, params object[] args)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            lock (LockObject)
            {
                if (_algorithms == null)
                {
                    Initialize();
                }
            }

            try
            {
                if (_algorithms == null) return null;
                var algoClass = _algorithms[name];
                // call the constructor for the type
                return Activator.CreateInstance(algoClass, args);
            }
            catch
            {
                // method doesn't throw any exception
                return null;
            }
        }
    }
}
