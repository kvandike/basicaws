﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace BasicAWS.Core.Cryptography
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract class HMAC : KeyedHashAlgorithm
    {

        private readonly bool _disposed;
        private string _hashName;
        private HashAlgorithm _algo;
        private BlockProcessor _block;

        // constructors

        protected HMAC()
        {
            _disposed = false;
            BlockSizeValue = 64;
        }

        // properties

        protected int BlockSizeValue { get; set; }

        public string HashName
        {
            get { return _hashName; }
            set
            {
                _hashName = value;
                _algo = HashAlgorithm.Create(_hashName);
            }
        }

        public override byte[] Key
        {
            get { return (byte[])base.Key.Clone(); }
            set
            {
                if ((value != null) && (value.Length > 64))
                    base.Key = _algo.ComputeHash(value);
                else if (value != null) base.Key = (byte[])value.Clone();
            }
        }

        internal BlockProcessor Block => _block ?? (_block = new BlockProcessor(_algo, (BlockSizeValue >> 3)));

        // methods

        private byte[] KeySetup(byte[] key, byte padding)
        {
            byte[] buf = new byte[BlockSizeValue];

            for (int i = 0; i < key.Length; ++i)
                buf[i] = (byte)(key[i] ^ padding);

            for (int i = key.Length; i < BlockSizeValue; ++i)
                buf[i] = padding;

            return buf;
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                base.Dispose(disposing);
            }
        }

        protected override void HashCore(byte[] rgb, int ib, int cb)
        {
            if (_disposed)
                throw new ObjectDisposedException("HMACSHA1");

            if (State == 0)
            {
                Initialize();
                State = 1;
            }
            Block.Core(rgb, ib, cb);
        }

        protected override byte[] HashFinal()
        {
            if (_disposed)
                throw new ObjectDisposedException("HMAC");
            State = 0;

            Block.Final();
            byte[] intermediate = _algo.Hash;

            byte[] buf = KeySetup(Key, 0x5C);
            _algo.Initialize();
            _algo.TransformBlock(buf, 0, buf.Length, buf, 0);
            _algo.TransformFinalBlock(intermediate, 0, intermediate.Length);
            byte[] hash = _algo.Hash;
            _algo.Initialize();
            // zeroize sensitive data
            Array.Clear(buf, 0, buf.Length);
            Array.Clear(intermediate, 0, intermediate.Length);
            return hash;
        }

        public override void Initialize()
        {
            if (_disposed)
                throw new ObjectDisposedException("HMAC");

            State = 0;
            Block.Initialize();
            byte[] buf = KeySetup(Key, 0x36);
            _algo.Initialize();
            Block.Core(buf);
            // zeroize key
            Array.Clear(buf, 0, buf.Length);
        }

        // static methods

        public static new HMAC Create()
        {
            return Create("System.Security.Cryptography.HMAC");
        }

        public static new HMAC Create(string algorithmName)
        {
            return (HMAC)CryptoConfig.CreateFromName(algorithmName);
        }
    }
}
