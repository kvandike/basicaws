﻿using System;

namespace BasicAWS.Core.Cryptography
{
    class BlockProcessor
    {
        private readonly ICryptoTransform _transform;
        private readonly byte[] _block;
        private readonly int _blockSize;  // in bytes (not in bits)
        private int _blockCount;

        public BlockProcessor(ICryptoTransform transform)
            : this(transform, transform.InputBlockSize)
        { }

        // some Transforms (like HashAlgorithm descendant) return 1 for
        // block size (which isn't their real internal block size)
        public BlockProcessor(ICryptoTransform transform, int blockSize)
        {
            _transform = transform;
            _blockSize = blockSize;
            _block = new byte[blockSize];
        }

        ~BlockProcessor()
        {
            // zeroize our block (so we don't retain any information)
            Array.Clear(_block, 0, _blockSize);
        }

        public void Initialize()
        {
            Array.Clear(_block, 0, _blockSize);
            _blockCount = 0;
        }

        public void Core(byte[] rgb)
        {
            Core(rgb, 0, rgb.Length);
        }

        public void Core(byte[] rgb, int ib, int cb)
        {
            // 1. fill the rest of the "block"
            int n = Math.Min(_blockSize - _blockCount, cb);
            Buffer.BlockCopy(rgb, ib, _block, _blockCount, n);
            _blockCount += n;

            // 2. if block is full then transform it
            if (_blockCount == _blockSize)
            {
                _transform.TransformBlock(_block, 0, _blockSize, _block, 0);

                // 3. transform any other full block in specified buffer
                int b = (cb - n) / _blockSize;
                for (int i = 0; i < b; i++)
                {
                    _transform.TransformBlock(rgb, n + ib, _blockSize, _block, 0);
                    n += _blockSize;
                }

                // 4. if data is still present fill the "block" with the remainder
                _blockCount = cb - n;
                if (_blockCount > 0)
                    Buffer.BlockCopy(rgb, n + ib, _block, 0, _blockCount);
            }
        }

        public byte[] Final()
        {
            return _transform.TransformFinalBlock(_block, 0, _blockCount);
        }
    }
}
