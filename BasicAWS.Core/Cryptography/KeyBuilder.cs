﻿using System;

namespace BasicAWS.Core.Cryptography
{
    internal static class KeyBuilder
    {

        private static Random _rng;

        private static Random Rng => _rng ?? (_rng = new Random());

        public static byte[] Key(int size)
        {
            var key = new byte[size];
            Rng.NextBytes(key);
            return key;
        }

        public static byte[] Iv(int size)
        {
            byte[] iv = new byte[size];
            Rng.NextBytes(iv);
            return iv;
        }
    }
}
