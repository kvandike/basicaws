﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using BasicAWS.Core.Interfaces;
using BasicAWS.Core.Signers;
using BasicAWS.Core.Util;

namespace BasicAWS.Core
{
    public class BasicAwsCalculate : IAwsCalculate
    {
        protected string AwsAccessKey { get; }
        protected string AwsSecretKey { get; }


        public BasicAwsCalculate(string awsAccessKey, string awsSecretKey)
        {
            AwsAccessKey = awsAccessKey;
            AwsSecretKey = awsSecretKey;
        }

        public CalculateData PresignedCalculate(string region, string bucketName, string objectKey)
        {
            Debug.WriteLine("PresignedUrl");
            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = $"-{region}";
            }

            var endpointUri = $"https://{bucketName}.s3{regionUrlPart}.amazonaws.com/{objectKey}";

            // construct the query parameter string to accompany the url
            var queryParams = new StringBuilder();
            // for SignatureV4, the max expiry for a presigned url is 7 days, expressed
            // in seconds
            var expiresOn = DateTime.UtcNow.AddDays(2);
            var period = Convert.ToInt64((expiresOn.ToUniversalTime() - DateTime.UtcNow).TotalSeconds);
            queryParams.AppendFormat("{0}={1}", Constant.X_Amz_Expires, ExtCore.UrlEncode(period.ToString()));

            var headers = new Dictionary<string, string>();

            var signer = new Aws4SignerForQueryParameterAuth
            {
                EndpointUri = new Uri(endpointUri),
                HttpMethod = "GET",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        queryParams.ToString(),
                                                        "UNSIGNED-PAYLOAD",
                                                        AwsAccessKey,
                                                        AwsSecretKey);
            headers.Add(Constant.Authorization, authorization);
            // build the presigned url to incorporate the authorization element
            var urlBuilder = new StringBuilder(endpointUri);

            // add our query params
            urlBuilder.AppendFormat("?{0}", queryParams);

            // and finally the Signature V4 authorization string components
            urlBuilder.AppendFormat("&{0}", authorization);
            Debug.WriteLine("\nComputed presigned url:\n{0}", urlBuilder);
            return new CalculateData(endpointUri)
            {
                Param = headers
            };
        }

        public CalculateData GetCalculate(string region, string bucketName, string objectKey)
        {
            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = $"-{region}";
            }

            var endpointUri = $"https://{bucketName}.s3{regionUrlPart}.amazonaws.com/{objectKey}";
            var uri = new Uri(endpointUri);

            // for a simple GET, we have no body so supply the precomputed 'empty' hash
            var headers = new Dictionary<string, string>
            {
                {Constant.X_Amz_Content_SHA256, Constant.EMPTY_BODY_SHA256}
            };

            var signer = new Aws4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "GET",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer
                .ComputeSignature(
                headers,
                "", // no query parameters
                Constant.EMPTY_BODY_SHA256,
                AwsAccessKey,
                AwsSecretKey);
            // place the computed signature into a formatted 'Authorization' header 
            // and call S3
            headers.Add(Constant.Authorization, authorization);
            return new CalculateData(endpointUri)
            {
                Param = headers
            };
        }

        public CalculateData PutCalculate(string region, string bucketName, string contentType, string objectName,
            byte[] data)
        {
            Debug.WriteLine("PutS3Object");
            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = $"-{region}";
            }

            var endpointUri = $"https://{bucketName}.s3{regionUrlPart}.amazonaws.com/{objectName}";
            var uri = new Uri(endpointUri);

            // precompute hash of the body content
            var contentHash = ExtCore.ComputeHash(data);
            var contentHashString = ExtCore.ToHexString(contentHash, true);

            var headers = new Dictionary<string, string>
            {
                {Constant.X_Amz_Content_SHA256, contentHashString},
                {Constant.ContentLenght, data.Length.ToString()},
                {Constant.ContentType, contentType}
            };

            var signer = new Aws4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "PUT",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer.ComputeSignature(
                headers,
                string.Empty, // no query parameters
                contentHashString,
                AwsAccessKey,
                AwsSecretKey);

            // express authorization for this as a header
            headers.Add(Constant.Authorization, authorization);
            return new CalculateData(endpointUri)
            {
                Param = headers
            };
        }

        public CalculateData PostCalculate(string region, string bucketName, string contentType, string objectName,
            byte[] data)
        {
            Debug.WriteLine("PostS3Object");
            // Virtual hosted style addressing places the bucket name into the host address
            var endpointUri = $"https://{bucketName}.s3{region}.amazonaws.com/{objectName}";

            // precompute hash of the body content
            var contentHash = ExtCore.ComputeHash(data);
            var contentHashString = ExtCore.ToHexString(contentHash, true);

            var signer = new Aws4SignerForPost
            {
                EndpointUri = new Uri(endpointUri),
                HttpMethod = "PUT",
                Service = "s3",
                Region = "us-west-2"
            };

            //var keyName = "SamplesPath/POSTedFile.txt";
            var dateTimeStamp = DateTime.UtcNow;

            // construct the policy document governing the POST; note we need to request data from
            // the signer to complete the document ahead of the actual signing. The policy does not
            // need newlines but the sample uses them to make the resulting document clearer to read.
            // The double {{ and }} are needed to escape the {} sequences in the document.
            var policyBuilder = new StringBuilder();

            policyBuilder.AppendFormat("{{ \"expiration\": \"{0}\"\n",
                dateTimeStamp.AddDays(1).ToString("2013-08-07T12:00:00.000Z"));
            policyBuilder.Append("\"conditions\" : [");
            policyBuilder.AppendFormat("{{ \"bucket\": \"{0}\"\n", bucketName);
            policyBuilder.AppendFormat("[ \"starts-with\", \"$key\", \"{0}\"]\n", objectName);
            policyBuilder.Append("{{ \"acl\" : \"public-read\" }\n");
            policyBuilder.AppendFormat(
                "{{ \"success_action_redirect\" : \"http://{0}.s3.amazonaws.com/successful_upload.html\" }}\n",
                bucketName);
            policyBuilder.Append("[ \"starts-with\", \"$Content-Type\", \"text/\"]\n");
            policyBuilder.AppendFormat("{{ \"{0}\" : \"14365123651274\" }}\n", Constant.X_Amz_Meta_UUID);
            policyBuilder.Append("[\"starts-with\", \"$x-amz-meta-tag\", \"\"]\n");

            // populate these with assistance from the signer
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\"}}\n", Constant.X_Amz_Credential,
                signer.FormatCredentialStringForPolicy(dateTimeStamp));
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\"}}\n", Constant.X_Amz_Algorithm,
                signer.FormatAlgorithmForPolicy);
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\" }}\n", Constant.X_Amz_Date,
                signer.FormatDateTimeForPolicy(dateTimeStamp));

            policyBuilder.Append("]\n}");

            // hash the Base64 version of the policy document and pass this to the signer as the body hash
            var policyStringBytes = Encoding.UTF8.GetBytes(policyBuilder.ToString());
            var base64PolicyString = Convert.ToBase64String(policyStringBytes);

            var headers = new Dictionary<string, string>
            {
                {Constant.X_Amz_Content_SHA256, contentHashString},
                {Constant.ContentLenght, data.Length.ToString()},
                {Constant.ContentType, contentType}
            };

            var authorization = signer.ComputeSignature(headers,
                "", // no query parameters
                base64PolicyString,
                AwsAccessKey,
                AwsSecretKey);
            headers.Add(Constant.Authorization, authorization);
            return new CalculateData(endpointUri)
            {
                Param = headers
            };
        }

        public CalculateData PutChunkedCalculate()
        {
            throw new NotImplementedException();
        }
    }
}
