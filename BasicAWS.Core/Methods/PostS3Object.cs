﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using BasicAWS.Core.Signers;
using BasicAWS.Core.Util;

namespace BasicAWS.Core.Methods
{
    /// <summary>
    /// Sample code showing how to POST objects to Amazon S3 with Signature V4 authorization
    /// </summary>
    public static class PostS3Object
    {
        static readonly string awsAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string awsSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];

        static readonly string objectContent =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tortor metus, sagittis eget augue ut,\n"
            + "feugiat vehicula risus. Integer tortor mauris, vehicula nec mollis et, consectetur eget tortor. In ut\n"
            + "elit sagittis, ultrices est ut, iaculis turpis. In hac habitasse platea dictumst. Donec laoreet tellus\n"
            + "at auctor tempus. Praesent nec diam sed urna sollicitudin vehicula eget id est. Vivamus sed laoreet\n"
            + "lectus. Aliquam convallis condimentum risus, vitae porta justo venenatis vitae. Phasellus vitae nunc\n"
            + "varius, volutpat quam nec, mollis urna. Donec tempus, nisi vitae gravida facilisis, sapien sem malesuada\n"
            + "purus, id semper libero ipsum condimentum nulla. Suspendisse vel mi leo. Morbi pellentesque placerat congue.\n"
            + "Nunc sollicitudin nunc diam, nec hendrerit dui commodo sed. Duis dapibus commodo elit, id commodo erat\n"
            + "congue id. Aliquam erat volutpat.\n";

        public static void Run(string region, string bucketName, string objectKey)
        {
            Debug.WriteLine("PostS3Object:\r\nposting object 'SamplesPath/POSTedFile.txt' from bucket 'mysamplesbucket' (us-west-2)");

            // Virtual hosted style addressing places the bucket name into the host address
            var endpointUri = new Uri("https://mysamplesbucket.s3-us-west-2.amazonaws.com/SamplesPath/POSTedFile.txt");

            // Note - if you wanted to use path style addressing, the endpoint above should
            //        be expressed as follows; it does not change the signing process 
            //var endpointUri = new Uri("https://s3-us-west-2.amazonaws.com/mysamplesbucket/SamplesPath/POSTedFile.txt");

            // precompute hash of the body content
            var contentHash = Aws4SignerBase.CanonicalRequestHashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(objectContent));
            var contentHashString = ExtCore.ToHexString(contentHash, true);

            var signer = new Aws4SignerForPost
            {
                EndpointUri = endpointUri,
                HttpMethod = "PUT",
                Service = "s3",
                Region = "us-west-2"
            };

            var keyName = "SamplesPath/POSTedFile.txt";

            var dateTimeStamp = DateTime.UtcNow;

            // construct the policy document governing the POST; note we need to request data from
            // the signer to complete the document ahead of the actual signing. The policy does not
            // need newlines but the sample uses them to make the resulting document clearer to read.
            // The double {{ and }} are needed to escape the {} sequences in the document.
            var policyBuilder = new StringBuilder();

            policyBuilder.AppendFormat("{{ \"expiration\": \"{0}\"\n", dateTimeStamp.AddDays(1).ToString("2013-08-07T12:00:00.000Z"));
            policyBuilder.Append("\"conditions\" : [");
            policyBuilder.AppendFormat("{{ \"bucket\": \"{0}\"\n", bucketName);
            policyBuilder.AppendFormat("[ \"starts-with\", \"$key\", \"{0}\"]\n", keyName);
            policyBuilder.Append("{{ \"acl\" : \"public-read\" }\n");
            policyBuilder.AppendFormat("{{ \"success_action_redirect\" : \"http://{0}.s3.amazonaws.com/successful_upload.html\" }}\n", bucketName);
            policyBuilder.Append("[ \"starts-with\", \"$Content-Type\", \"text/\"]\n");
            policyBuilder.AppendFormat("{{ \"{0}\" : \"14365123651274\" }}\n", Aws4SignerBase.X_Amz_Meta_UUID);
            policyBuilder.Append("[\"starts-with\", \"$x-amz-meta-tag\", \"\"]\n");

            // populate these with assistance from the signer
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\"}}\n", Aws4SignerBase.X_Amz_Credential, signer.FormatCredentialStringForPolicy(dateTimeStamp));
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\"}}\n", Aws4SignerBase.X_Amz_Algorithm, signer.FormatAlgorithmForPolicy);
            policyBuilder.AppendFormat("{{ \"{0}\" : \"{1}\" }}\n", Aws4SignerBase.X_Amz_Date, signer.FormatDateTimeForPolicy(dateTimeStamp));

            policyBuilder.Append("]\n}");

            // hash the Base64 version of the policy document and pass this to the signer as the body hash
            var policyStringBytes = Encoding.UTF8.GetBytes(policyBuilder.ToString());
            var base64PolicyString = System.Convert.ToBase64String(policyStringBytes);

            var headers = new Dictionary<string, string>
            {
                {Aws4SignerBase.X_Amz_Content_SHA256, contentHashString},
                {"content-length", objectContent.Length.ToString()},
                {"content-type", "text/plain"}
            };

            var authorization = signer.ComputeSignature(headers,
                                                        "",   // no query parameters
                                                        base64PolicyString,
                                                        awsAccessKey,
                                                        awsSecretKey);

        }
    }
}
