﻿using System;
using System.Collections.Generic;
using BasicAWS.Core.Signers;

namespace BasicAWS.Core.Methods
{
    /// <summary>
    /// Samples showing how to GET an object from Amazon S3 using Signature V4 authorization.
    /// </summary>
    public class GetS3Object
    {

        private Aws4SignerBase _aws4SignerBase;


        public GetS3Object(Aws4SignerBase aws4SignerBase)
        {
            _aws4SignerBase = aws4SignerBase;
        }

        /// <summary>
        /// Request the content of an object from the specified bucket using virtual hosted-style 
        /// object addressing.
        /// </summary>
        public void Run(string region, string bucketName, string objectKey, string awsAccessKey, string awsSecretKey)
        {

            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = string.Format("-{0}", region);
            }

            var endpointUri = string.Format("https://{0}.s3{1}.amazonaws.com/{2}",
                                               bucketName,
                                               regionUrlPart,
                                               objectKey);
            var uri = new Uri(endpointUri);

            // for a simple GET, we have no body so supply the precomputed 'empty' hash
            var headers = new Dictionary<string, string>
            {
                {Aws4SignerBase.X_Amz_Content_SHA256, Aws4SignerBase.EMPTY_BODY_SHA256},
                {"content-type", "text/plain"}
            };

            var signer = new Aws4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "GET",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        "",   // no query parameters
                                                        Aws4SignerBase.EMPTY_BODY_SHA256,
                                                        awsAccessKey,
                                                        awsSecretKey);

            // place the computed signature into a formatted 'Authorization' header 
            // and call S3
            headers.Add("Authorization", authorization);

            HttpHelpers.InvokeHttpRequest(uri, "GET", headers, null);
        }
    }
}
