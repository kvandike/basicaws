﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using BasicAWS.Core.Signers;
using BasicAWS.Core.Util;

namespace BasicAWS.Core.Methods
{
    /// <summary>
    /// Sample code showing how to PUT objects to Amazon S3 with Signature V4 authorization
    /// </summary>
    public static class PutS3Object
    {
        static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];

        private const string ObjectContent 
            = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tortor metus, sagittis eget augue ut,\n" 
            + "feugiat vehicula risus. Integer tortor mauris, vehicula nec mollis et, consectetur eget tortor. In ut\n" 
            + "elit sagittis, ultrices est ut, iaculis turpis. In hac habitasse platea dictumst. Donec laoreet tellus\n" 
            + "at auctor tempus. Praesent nec diam sed urna sollicitudin vehicula eget id est. Vivamus sed laoreet\n" 
            + "lectus. Aliquam convallis condimentum risus, vitae porta justo venenatis vitae. Phasellus vitae nunc\n" 
            + "varius, volutpat quam nec, mollis urna. Donec tempus, nisi vitae gravida facilisis, sapien sem malesuada\n" 
            + "purus, id semper libero ipsum condimentum nulla. Suspendisse vel mi leo. Morbi pellentesque placerat congue.\n" 
            + "Nunc sollicitudin nunc diam, nec hendrerit dui commodo sed. Duis dapibus commodo elit, id commodo erat\n" 
            + "congue id. Aliquam erat volutpat.\n";

        /// <summary>
        /// Uploads content to an Amazon S3 object in a single call using Signature V4 authorization.
        /// </summary>
        public static void Run(string region, string bucketName, string objectKey)
        {
            Debug.WriteLine("PutS3Object");

            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = string.Format("-{0}", region);
            }

            var endpointUri = string.Format("https://{0}.s3{1}.amazonaws.com/{2}",
                                               bucketName,
                                               regionUrlPart,
                                               objectKey);
            var uri = new Uri(endpointUri);

            // precompute hash of the body content
            var contentHash = Aws4SignerBase.CanonicalRequestHashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(ObjectContent));
            var contentHashString = ExtCore.ToHexString(contentHash, true);

            var headers = new Dictionary<string, string>
            {
                {Aws4SignerBase.X_Amz_Content_SHA256, contentHashString},
                {"content-length", ObjectContent.Length.ToString()},
                {"content-type", "text/plain"}
            };

            var signer = new Aws4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "PUT",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        "",   // no query parameters
                                                        contentHashString,
                                                        AWSAccessKey,
                                                        AWSSecretKey);

            // express authorization for this as a header
            headers.Add("Authorization", authorization);

            // make the call to Amazon S3
            HttpHelpers.InvokeHttpRequest(uri, "PUT", headers, ObjectContent);
        }
    }
}
