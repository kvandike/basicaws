﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using BasicAWS.Core.Signers;

namespace BasicAWS.Core.Methods
{
    /// <summary>
    /// Sample code showing how to use Presigned Urls with Signature V4 authorization
    /// </summary>
    public class PresignedUrl
    {
        static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];

        /// <summary>
        /// Construct a basic presigned url to the object '/SamplesPath/MySampleFile.txt' 
        /// in the bucket 'mysamplesbucket' in us-west-2 using path-style object addressing.
        /// The signature V4 authorization data is embedded in the url as query parameters.
        /// </summary>
        public static void Run(string region, string bucketName, string objectKey)
        {
            Debug.WriteLine("PresignedUrl");

            // Construct a virtual hosted style address with the bucket name part of the host address,
            // placing the region into the url if we're not using us-east-1.
            var regionUrlPart = string.Empty;
            if (!string.IsNullOrEmpty(region))
            {
                if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
                    regionUrlPart = string.Format("-{0}", region);
            }

            var endpointUri = string.Format("https://{0}.s3{1}.amazonaws.com/{2}",
                                               bucketName,
                                               regionUrlPart,
                                               objectKey);

            // construct the query parameter string to accompany the url
            var queryParams = new StringBuilder();
            // for SignatureV4, the max expiry for a presigned url is 7 days, expressed
            // in seconds
            var expiresOn = DateTime.UtcNow.AddDays(2);
            var period = Convert.ToInt64((expiresOn.ToUniversalTime() - DateTime.UtcNow).TotalSeconds);
            queryParams.AppendFormat("{0}={1}", Aws4SignerBase.X_Amz_Expires, HttpHelpers.UrlEncode(period.ToString()));

            var headers = new Dictionary<string, string>();

            var signer = new Aws4SignerForQueryParameterAuth
            {
                EndpointUri = new Uri(endpointUri),
                HttpMethod = "GET",
                Service = "s3",
                Region = "us-west-2"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        queryParams.ToString(),
                                                        "UNSIGNED-PAYLOAD",
                                                        AWSAccessKey,
                                                        AWSSecretKey);

            // build the presigned url to incorporate the authorization element
            var urlBuilder = new StringBuilder(endpointUri.ToString());

            // add our query params
            urlBuilder.AppendFormat("?{0}", queryParams.ToString());

            // and finally the Signature V4 authorization string components
            urlBuilder.AppendFormat("&{0}", authorization);

            var presignedUrl = urlBuilder.ToString();
            Debug.WriteLine("\nComputed presigned url:\n{0}", presignedUrl);
            
        }
    }
}
