﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using BasicAWS.Core.Signers;

namespace BasicAWS.Core.Methods
{
    /// <summary>
    /// Sample code showing how to PUT objects to Amazon S3 using chunked uploading 
    /// with Signature V4 authorization
    /// </summary>
    //public static class PutS3ObjectChunked
    //{
    //    static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
    //    static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];

    //    /// <summary>
    //    /// Uploads content to an Amazon S3 object in a series of signed 'chunks' using Signature V4 authorization.
    //    /// </summary>
    //    public static void Run(string region, string bucketName, string objectKey)
    //    {
    //        Debug.WriteLine("PutS3ObjectChunked");

    //        // this sample uses a chunk data length of 64K; this will yield one 64K chunk,
    //        // one partial chunk and the final 0 byte payload terminator chunk
    //        const int userDataBlockSize = 65536;
    //        var sampleContent = Make65KPayload();

    //        // Construct a virtual hosted style address with the bucket name part of the host address,
    //        // placing the region into the url if we're not using us-east-1.
    //        var regionUrlPart = string.Empty;
    //        if (!string.IsNullOrEmpty(region))
    //        {
    //            if (!region.Equals("us-east-1", StringComparison.OrdinalIgnoreCase))
    //                regionUrlPart = string.Format("-{0}", region);
    //        }

    //        var endpointUri = string.Format("https://{0}.s3{1}.amazonaws.com/{2}",
    //                                           bucketName,
    //                                           regionUrlPart,
    //                                           objectKey);

    //        // set the markers indicating we're going to send the upload as a series 
    //        // of chunks:
    //        //   -- 'x-amz-content-sha256' is the fixed marker indicating chunked
    //        //      upload
    //        //   -- 'content-length' becomes the total size in bytes of the upload 
    //        //      (including chunk headers), 
    //        //   -- 'x-amz-decoded-content-length' is used to transmit the actual 
    //        //      length of the data payload, less chunk headers
    //        var headers = new Dictionary<string, string>
    //        {
    //            {Aws4SignerBase.X_Amz_Content_SHA256, Aws4SignerForChunkedUpload.STREAMING_BODY_SHA256},
    //            {"content-encoding", "aws-chunked"},
    //            {"content-type", "text/plain"},
    //            {Aws4SignerBase.X_Amz_Decoded_Content_Length, sampleContent.Length.ToString()}
    //        };

    //        var signer = new Aws4SignerForChunkedUpload
    //        {
    //            EndpointUri = new Uri(endpointUri),
    //            HttpMethod = "PUT",
    //            Service = "s3",
    //            Region = "us-west-2"
    //        };

    //        // how big is the overall request stream going to be once we add the signature 
    //        // 'headers' to each chunk?
    //        var totalLength = signer.CalculateChunkedContentLength(sampleContent.Length,
    //                                                               userDataBlockSize);
    //        headers.Add("content-length", totalLength.ToString());

    //        var authorization = signer.ComputeSignature(headers,
    //                                                    "",   // no query parameters
    //                                                    Aws4SignerForChunkedUpload.STREAMING_BODY_SHA256,
    //                                                    AWSAccessKey,
    //                                                    AWSSecretKey);

    //        // place the computed signature into a formatted 'Authorization' header 
    //        // and call S3
    //        headers.Add("Authorization", authorization);

    //        // start consuming the data payload in blocks which we subsequently chunk; this prefixes
    //        // the data with a 'chunk header' containing signature data from the prior chunk (or header
    //        // signing, if the first chunk) plus length and other data. Each completed chunk is
    //        // written to the request stream and to complete the upload, we send a final chunk with
    //        // a zero-length data payload.

    //        try
    //        {
    //            // first, set up the request
    //            var request = HttpHelpers.ConstructWebRequest(new Uri(endpointUri), "PUT", headers,sampleContent);

    //            // get the request stream and start writing the user data as chunks, as outlined
    //            // above; as 
    //            var buffer = new byte[userDataBlockSize];
    //            var requestStream = request.GetRequestStream();
    //            using (var inputStream = new MemoryStream(Encoding.UTF8.GetBytes(sampleContent)))
    //            {
    //                var bytesRead = 0;
    //                while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) > 0)
    //                {
    //                    // process into a chunk
    //                    var chunk = signer.ConstructSignedChunk(bytesRead, buffer);

    //                    // send the chunk
    //                    requestStream.Write(chunk, 0, chunk.Length);
    //                }
    //                // last step is to send a signed zero-length chunk to complete the upload
    //                var finalchunk = signer.ConstructSignedChunk(0, buffer);
    //                requestStream.Write(finalchunk, 0, finalchunk.Length);
    //                requestStream.Close();
    //                HttpHelpers.CheckResponse(request);
    //            }
    //        }
    //        catch (WebException ex)
    //        {
    //            using (var response = ex.Response as HttpWebResponse)
    //            {
    //                if (response != null)
    //                {
    //                    var errorMsg = HttpHelpers.ReadResponseBody(response);
    //                    Debug.WriteLine("Error:{0}", errorMsg);
    //                    Debug.WriteLine("Unexpected status code returned: {0}", response.StatusCode);
    //                }
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Want sample to upload 3 chunks for our selected chunk size of 64K; one full
    //    /// size chunk, one partial chunk and then the 0-byte terminator chunk. This routine
    //    /// just takes 1K of seed text and turns it into a 65K-or-so string for sample use.
    //    /// </summary>
    //    /// <returns></returns>
    //    static string Make65KPayload()
    //    {
    //        const string contentSeed 
    //            = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tortor metus, sagittis eget augue ut,\n"
    //                + "feugiat vehicula risus. Integer tortor mauris, vehicula nec mollis et, consectetur eget tortor. In ut\n"
    //                + "elit sagittis, ultrices est ut, iaculis turpis. In hac habitasse platea dictumst. Donec laoreet tellus\n"
    //                + "at auctor tempus. Praesent nec diam sed urna sollicitudin vehicula eget id est. Vivamus sed laoreet\n"
    //                + "lectus. Aliquam convallis condimentum risus, vitae porta justo venenatis vitae. Phasellus vitae nunc\n"
    //                + "varius, volutpat quam nec, mollis urna. Donec tempus, nisi vitae gravida facilisis, sapien sem malesuada\n"
    //                + "purus, id semper libero ipsum condimentum nulla. Suspendisse vel mi leo. Morbi pellentesque placerat congue.\n"
    //                + "Nunc sollicitudin nunc diam, nec hendrerit dui commodo sed. Duis dapibus commodo elit, id commodo erat\n"
    //                + "congue id. Aliquam erat volutpat.\n";

    //        var oneKSeed = new StringBuilder();
    //        while (oneKSeed.Length < 1024)
    //        {
    //            oneKSeed.AppendFormat("{0}\n", contentSeed);
    //        }
 
    //        // now scale up to meet/exceed our requirement
    //        var output = new StringBuilder();
    //        for (int i = 0; i < 66; i++)
    //        {
    //            output.AppendFormat("{0}", oneKSeed);
    //        }

    //        return output.ToString();
    //    }
    //}
}
