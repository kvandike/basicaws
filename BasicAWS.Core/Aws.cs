namespace BasicAWS.Core
{
    public class Aws
    {
        private readonly string _awsAccessKey;
        private readonly string _awsSecretKey;
        private readonly string _region;
        private readonly string _bucketName;

        public Aws(string awsAccessKey, string awsSecretKey, string region, string bucketName)
        {
            _awsAccessKey = awsAccessKey;
            _awsSecretKey = awsSecretKey;
            _region = region;
            _bucketName = bucketName;
        }

        public CalculateData PutCalculate(string contentType, string objectName, byte[] data)
        {

            var putCalculate = new BasicAwsCalculate(_awsAccessKey, _awsSecretKey);
            return putCalculate.PutCalculate(_region, _bucketName, contentType, objectName, data);
        }

        public CalculateData GetCalculate(string objectName)
        {
            var getCalculate = new BasicAwsCalculate(_awsAccessKey,_awsSecretKey);
            return getCalculate.GetCalculate(_region, _bucketName, objectName);
        }
    }
}