﻿using System.Collections.Generic;

namespace BasicAWS.Core
{
    public class CalculateData
    {
        private IDictionary<string, string> _param;

        public string EndpointUri { get; }

        public IDictionary<string, string> Param
        {
            get { return _param ?? (_param = new Dictionary<string, string>()); }
            set { _param = value; }
        }

        public CalculateData(string endpointUri)
        {
            EndpointUri = endpointUri;
        }
    }
}
