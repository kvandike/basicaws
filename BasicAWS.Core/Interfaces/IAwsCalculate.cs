﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicAWS.Core.Interfaces
{
    public interface IAwsCalculate
    {

        CalculateData PresignedCalculate(string region, string bucketName, string objectKey);

        CalculateData GetCalculate(string region, string bucketName, string objectKey);

        CalculateData PutCalculate(string region, string bucketName, string contentType, string objectName, byte[] data);

        CalculateData PostCalculate(string region, string bucketName, string contentType, string objectName, byte[] data);

        CalculateData PutChunkedCalculate();
    }
}
