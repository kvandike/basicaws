﻿using System.Net.Http;
using System.Threading.Tasks;

namespace BasicAWS.Core.Interfaces
{
    public interface IAwsClient<T>
    {
        Task<T> GetResource(string objectName);

        Task<T> PutResource(string contentType, string objectName, byte[] data);
    }
}
