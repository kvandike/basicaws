﻿using System.Collections.Generic;

namespace BasicAWS.Core.Interfaces
{
    public interface IComputeSignature
    {
        string ComputeSignature(
            IDictionary<string, string> headers,
            string queryParameters,
            string bodyHash,
            string awsAccessKey,
            string awsSecretKey);
    }
}
