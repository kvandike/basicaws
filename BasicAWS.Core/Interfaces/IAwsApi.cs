﻿namespace BasicAWS.Core.Interfaces
{
    public interface IAwsApi
    {

        /// <summary>
        /// algorithm used to hash the canonical request that is supplied to
        /// the signature computation
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        byte[] ComputeHash(byte[] input);


        /// <summary>
        /// Compute and return the hash of a data blob using the specified algorithm
        /// and key
        /// </summary>
        /// <param name="algorithm">Algorithm to use for hashing</param>
        /// <param name="key">Hash key</param>
        /// <param name="data">Data blob</param>
        /// <returns>Hash of the data</returns>
        byte[] ComputeKeyedHash(string algorithm, byte[] key, byte[] data);
    }
}
