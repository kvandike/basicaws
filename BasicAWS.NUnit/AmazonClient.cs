﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BasicAWS.NUnit
{
    public class AmazonClient
    {

        // 7 days is the maximum period for presigned url expiry with AWS4
        public const Int64 MaxAWS4PreSignedUrlExpiry = 7 * 24 * 60 * 60;

        public const string Scheme = "AWS4";
        internal const string XAmzSignature = "X-Amz-Signature";
        internal const string XAmzAlgorithm = "X-Amz-Algorithm";
        internal const string XAmzCredential = "X-Amz-Credential";
        internal const string XAmzDate = "X-Amz-Date";
        internal const string XAmzExpires = "X-Amz-Expires";
        public const string Algorithm = "HMAC-SHA256";



    }
}
