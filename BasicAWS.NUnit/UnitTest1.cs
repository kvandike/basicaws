﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BasicAWS.Android;
using BasicAWS.Core;
using BasicAWS.Core.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModernHttpClient;

namespace BasicAWS.NUnit
{
    [TestClass]
    public class UnitTest1
    {
        private const string ObjectContent
    = "11 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tortor metus, sagittis eget augue ut,\n"
    + "feugiat vehicula risus. Integer tortor mauris, vehicula nec mollis et, consectetur eget tortor. In ut\n"
    + "elit sagittis, ultrices est ut, iaculis turpis. In hac habitasse platea dictumst. Donec laoreet tellus\n"
    + "at auctor tempus. Praesent nec diam sed urna sollicitudin vehicula eget id est. Vivamus sed laoreet\n"
    + "lectus. Aliquam convallis condimentum risus, vitae porta justo venenatis vitae. Phasellus vitae nunc\n"
    + "varius, volutpat quam nec, mollis urna. Donec tempus, nisi vitae gravida facilisis, sapien sem malesuada\n"
    + "purus, id semper libero ipsum condimentum nulla. Suspendisse vel mi leo. Morbi pellentesque placerat congue.\n"
    + "Nunc sollicitudin nunc diam, nec hendrerit dui commodo sed. Duis dapibus commodo elit, id commodo erat\n"
    + "congue id. Aliquam erat volutpat.\n";


        private static string AccessKey => File.ReadAllText(@"C:\Users\aliko\OneDrive\Проект\BasicAWS\Keys\TestAccessKey.txt");
        private static string SecretKey => File.ReadAllText(@"C:\Users\aliko\OneDrive\Проект\BasicAWS\Keys\TestSecretKey.txt");
        private static string BucketName = "com.kvandike.test2";
        private static string Region = "us-east-1";
        private static string TestContent = "text/plain";
        private static string TestTextName = "MySampleFile2.txt";
        private static string TestImageName = "MySampleFile.png";




        //[TestMethod]
        public async Task PutTestMethod1()
        {
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsBuilder();
            var resource = builder.CreateAws( AccessKey, SecretKey, Region, BucketName);
            var requestData = Encoding.UTF8.GetBytes(ObjectContent);
            var data = resource.Put(TestContent, "MySampleFile2.txt", requestData);
            var  client = new HttpClient(new NativeMessageHandler());
            var request = new HttpRequestMessage(HttpMethod.Put, data.EndpointUri);
            request.Content = new ByteArrayContent(requestData);
            request.Method = HttpMethod.Put;
            foreach (var param in data.Param.Keys)
            {
                switch (param)
                {
                    case Constant.Authorization:
                        client.DefaultRequestHeaders.TryAddWithoutValidation(param, data.Param[param]);    
                        break;
                    case Constant.ContentLenght:
                        request.Content.Headers.ContentLength = long.Parse(data.Param[param]);
                        break;
                    case Constant.ContentType:
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue(data.Param[param]);
                        break;
                    case Constant.Host:
                        request.Headers.Host = data.Param[param];
                        break;
                    default:
                        request.Headers.Add(param, data.Param[param]);
                        break;;
                }
            }
            var response = await client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            int h = 0;
        }

        //[TestMethod]
        public async Task GetTestMethod1()
        {
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsBuilder();
            var resource = builder.CreateAws(AccessKey, SecretKey, Region, BucketName);
            var data = resource.Get("MySampleFile2.txt");
            var client = new HttpClient(new NativeMessageHandler());
            var request = new HttpRequestMessage(HttpMethod.Get, data.EndpointUri);
            request.Method = HttpMethod.Get;
            foreach (var param in data.Param.Keys)
            {
                switch (param)
                {
                    case Constant.Authorization:
                        request.Headers.TryAddWithoutValidation(param, data.Param[param]);
                        break;
                    case Constant.Host:
                        request.Headers.Host = data.Param[param];
                        break;
                    default:
                        request.Headers.TryAddWithoutValidation(param, data.Param[param]);
                        break; 
                }
            }
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(TestContent));
            var response = await client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            int h = 0;
        }

       //[TestMethod]
        public async Task GetNewcase()
        {
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsBuilder();
            var resource = builder.CreateAws(AccessKey, SecretKey, Region, BucketName);
            var data = resource.Get(TestImageName);
            var requestHelper = new BasicHttpRequestHelper();
            var request = requestHelper.GetMessage(data);
            using (var client = new HttpClient(new NativeMessageHandler()))
            {
                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsByteArrayAsync();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("--- Complete Get ---");
                    string testPath = @"D:\Users\aliko\Pictures\Screenshots\test1.png";
                    File.WriteAllBytes(testPath,content);
                    //Console.WriteLine(content);
                }
            }
        }

        //[TestMethod]
        public async Task PutNewcase()
        {
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsBuilder();
            var requestData = File.ReadAllBytes(@"D:\Users\aliko\Pictures\Screenshots\test.png");
            var resource = builder.CreateAws(AccessKey, SecretKey, Region, BucketName);
            var data = resource.Put(TestContent, TestImageName, requestData);
            var requestHelper = new BasicHttpRequestHelper();
            var request = requestHelper.PutMessage(data, requestData);
            using (var client = new HttpClient(new NativeMessageHandler()))
            {
                var response = await client.SendAsync(request);
                var content = await response.Content.ReadAsStringAsync();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("--- Complete Put ---");
                    Console.WriteLine(content);
                }
            }
        }


        //[TestMethod]
        public async Task WebRequestCase()
        {
            var builder = new AwsBuilder();
            var resource = builder.CreateAws(AccessKey, SecretKey, Region, BucketName);
            var requestData = Encoding.UTF8.GetBytes(ObjectContent);
            var data = resource.Put(TestContent, "MySampleFile2.txt", requestData);
            InvokeHttpRequest(new Uri(data.EndpointUri), "PUT", data.Param, ObjectContent);
        }

        [TestMethod]
        public async Task AndroidGetCase()
        {
            var accessKey = AccessKey;
            var secretKey = SecretKey;
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsClientBuilder();
            var resource = builder.CreateAwsClient(accessKey, secretKey, Region, BucketName);
            var response =await resource.Get(TestImageName);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
        }

        //[TestMethod]
        public async Task AndroidPutCase()
        {
            var accessKey = AccessKey;
            var secretKey = SecretKey;
            ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
            var builder = new AwsClientBuilder();
            var resource = builder.CreateAwsClient(accessKey, secretKey, Region, BucketName);
            var requestData = Encoding.UTF8.GetBytes(ObjectContent);
            var response = await resource.Put(TestContent, TestImageName, requestData);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
        }


        /// <summary>
        /// Construct a HttpWebRequest onto the specified endpoint and populate
        /// the headers.
        /// </summary>
        /// <param name="endpointUri">The endpoint to call</param>
        /// <param name="httpMethod">GET, PUT etc</param>
        /// <param name="headers">The set of headers to apply to the request</param>
        /// <returns>Initialized HttpWebRequest instance</returns>
        public static HttpWebRequest ConstructWebRequest(Uri endpointUri,
            string httpMethod,
            IDictionary<string, string> headers, string requestBody)
        {
            var request = (HttpWebRequest)WebRequest.Create(endpointUri);
            request.Method = httpMethod;

            foreach (var header in headers.Keys)
            {
                // not all headers can be set via the dictionary
                if (header.Equals(Constant.Host, StringComparison.OrdinalIgnoreCase))
                    request.Host = headers[header];
                else if (header.Equals(Constant.ContentLenght, StringComparison.OrdinalIgnoreCase))
                {
                    var buffer = Encoding.ASCII.GetBytes(requestBody);
                    //request.ContentLength = buffer.Length;
                    request.ContentLength = long.Parse(headers[header]);
                }
                else if (header.Equals(Constant.ContentType, StringComparison.OrdinalIgnoreCase))
                    request.ContentType = headers[header];
                else
                    request.Headers.Add(header, headers[header]);
            }

            return request;
        }

        /// <summary>
        /// Makes a http request to the specified endpoint
        /// </summary>
        /// <param name="endpointUri"></param>
        /// <param name="httpMethod"></param>
        /// <param name="headers"></param>
        /// <param name="requestBody"></param>
        public static void InvokeHttpRequest(Uri endpointUri,
            string httpMethod,
            IDictionary<string, string> headers,
            string requestBody)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => true;
                var request = ConstructWebRequest(endpointUri, httpMethod, headers, requestBody);

                if (!string.IsNullOrEmpty(requestBody))
                {
                    var buffer = new byte[8192]; // arbitrary buffer size                        
                    var requestStream = request.GetRequestStream();
                    using (var inputStream = new MemoryStream(Encoding.UTF8.GetBytes(requestBody)))
                    {
                        var bytesRead = 0;
                        while ((bytesRead = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            requestStream.Write(buffer, 0, bytesRead);
                        }
                    }
                    requestStream.Close();
                }

                CheckResponse(request);
            }
            catch (WebException ex)
            {
                using (var response = ex.Response as HttpWebResponse)
                {
                    if (response != null)
                    {
                        var errorMsg = ReadResponseBody(response);
                        Console.WriteLine("\n-- HTTP call failed with exception '{0}', status code '{1}'", errorMsg,
                            response.StatusCode);
                    }
                }
            }
        }

        public static void CheckResponse(HttpWebRequest request)
        {

            // Get the response and read any body into a string, then display.
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("\n-- HTTP call succeeded");
                    var responseBody = ReadResponseBody(response);
                    if (!string.IsNullOrEmpty(responseBody))
                    {
                        Console.WriteLine("\n-- Response body:");
                        Console.WriteLine(responseBody);
                    }
                }
                else
                    Console.WriteLine("\n-- HTTP call failed, status code: {0}", response.StatusCode);
            }
        }

        /// <summary>
        /// Reads the response data from the service call, if any
        /// </summary>
        /// <param name="response">
        /// The response instance obtained from the previous request
        /// </param>
        /// <returns>The body content of the response</returns>
        public static string ReadResponseBody(HttpWebResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response", "Value cannot be null");

            // Then, open up a reader to the response and read the contents to a string
            // and return that to the caller.
            string responseBody = string.Empty;
            using (var responseStream = response.GetResponseStream())
            {
                if (responseStream != null)
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseBody = reader.ReadToEnd();
                    }
                }
            }
            return responseBody;
        }
    }
}
