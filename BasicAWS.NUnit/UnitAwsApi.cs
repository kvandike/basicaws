using System.Security.Cryptography;
using BasicAWS.Core.Interfaces;

namespace BasicAWS.Android
{
    public class UnitAwsApi : IAwsApi
    {

        private readonly HashAlgorithm _canonicalRequestHashAlgorithm = HashAlgorithm.Create("SHA-256");

        public byte[] ComputeHash(byte[] input)
        {
            return _canonicalRequestHashAlgorithm.ComputeHash(input);
        }

        public byte[] ComputeKeyedHash(string algorithm, byte[] key, byte[] data)
        {
            var kha = KeyedHashAlgorithm.Create(algorithm);
            kha.Key = key;
            return kha.ComputeHash(data);
        }
    }
}