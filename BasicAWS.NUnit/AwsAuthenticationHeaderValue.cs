﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BasicAWS.NUnit
{
    class AwsAuthenticationHeaderValue : AuthenticationHeaderValue
    {
        public AwsAuthenticationHeaderValue(string scheme) : base(scheme)
        {
        }

        public AwsAuthenticationHeaderValue(string scheme, string parameter) : base(scheme, parameter)
        {
        }
    }
}
