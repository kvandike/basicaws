﻿using System.Net.Http;
using System.Net.Http.Headers;
using BasicAWS.Core;
using BasicAWS.Core.Util;

namespace BasicAWS.Android
{
    public class BasicHttpRequestHelper
    {

        public HttpRequestMessage GetMessage(CalculateData data)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, data.EndpointUri) {Method = HttpMethod.Get};
            foreach (var param in data.Param.Keys)
            {
                switch (param)
                {
                    case Constant.Host:
                        request.Headers.Host = data.Param[param];
                        break;
                    default:
                        request.Headers.TryAddWithoutValidation(param, data.Param[param]);
                        break;
                }
            }
            return request;
        }


        public HttpRequestMessage PutMessage(CalculateData data,byte[] requestBody)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, data.EndpointUri)
            {
                Content = new ByteArrayContent(requestBody),
                Method = HttpMethod.Put
            };
            foreach (var param in data.Param.Keys)
            {
                switch (param)
                {
                    case Constant.ContentLenght:
                        request.Content.Headers.ContentLength = long.Parse(data.Param[param]);
                        break;
                    case Constant.ContentType:
                        request.Content.Headers.ContentType = new MediaTypeHeaderValue(data.Param[param]);
                        break;
                    case Constant.Host:
                        request.Headers.Host = data.Param[param];
                        break;
                    default:
                        request.Headers.TryAddWithoutValidation(param, data.Param[param]);
                        break;
                }
            }
            return request;
        }

    }
}
