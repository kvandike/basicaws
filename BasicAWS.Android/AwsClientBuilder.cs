using System.Net.Http;
using System.Threading.Tasks;

namespace BasicAWS.Android
{
    public class AwsClientBuilder
    {
        public AwsClientResource CreateAwsClient(string awsAccessKey, string awsSecretKey, string region, string bucketName)
        {
            var aws = new AwsClient(awsAccessKey, awsSecretKey, region, bucketName);
            return new AwsClientResource(aws);
        }

        public class AwsClientResource
        {
            private readonly AwsClient _awsClient;

            internal AwsClientResource(AwsClient awsClient)
            {
                _awsClient = awsClient;
            }


            public async Task<HttpResponseMessage> Put(string contentType, string objectName, byte[] data)
            {
                return await _awsClient.PutResource(contentType, objectName, data);
            }


            public async Task<HttpResponseMessage> Get(string objectName)
            {
               return await _awsClient.GetResource(objectName);
            }



        }
    }
}