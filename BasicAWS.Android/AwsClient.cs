using System.Net.Http;
using System.Threading.Tasks;
using BasicAWS.Core;
using BasicAWS.Core.Interfaces;
using ModernHttpClient;

namespace BasicAWS.Android
{
    public class AwsClient :  IAwsClient<HttpResponseMessage>
    {
        private readonly string _awsAccessKey;
        private readonly string _awsSecretKey;
        private readonly string _region;
        private readonly string _bucketName;

        public AwsClient(string awsAccessKey, string awsSecretKey, string region, string bucketName)
        {
            _awsAccessKey = awsAccessKey;
            _awsSecretKey = awsSecretKey;
            _region = region;
            _bucketName = bucketName;
        }

        public async Task<HttpResponseMessage> GetResource(string objectName)
        {
            using (var client = new HttpClient(new NativeMessageHandler()))
            {
                var getCalculate = new BasicAwsCalculate(_awsAccessKey, _awsSecretKey);
                var data = getCalculate.GetCalculate(_region, _bucketName, objectName);
                var requestHelper = new BasicHttpRequestHelper();
                var request = requestHelper.GetMessage(data);
                return await client.SendAsync(request);
            }
        }

        public async Task<HttpResponseMessage> PutResource(string contentType, string objectName, byte[] data)
        {
            using (var client = new HttpClient(new NativeMessageHandler()))
            {
                var putCalculate = new BasicAwsCalculate(_awsAccessKey, _awsSecretKey);
                var calculate = putCalculate.PutCalculate(_region, _bucketName, contentType, objectName, data);
                var requestHelper = new BasicHttpRequestHelper();
                var request = requestHelper.PutMessage(calculate, data);
                return await client.SendAsync(request);
            }
        }
    }
}